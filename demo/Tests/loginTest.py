import os
import sys
import time
import unittest

from selenium import webdriver

sys.path.append(os.path.join(os.path.dirname(__file__),"..",".."))
from demo.POMPackage.LoginPage import loginPage
from demo.POMPackage.SignUpPage import signupPage
import HTMLTestRunner


class LoginTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome("E:\\Python37-32\\Lib\site-packages\\selenium\\webdriver\\chrome\\chromedriver.exe")
        cls.driver.set_page_load_timeout(30)
        cls.driver.maximize_window()
        cls.driver.implicitly_wait(30)

    def test_login_page(self):
        driver = self.driver
        driver.get("https://www.facebook.com/")
        login = loginPage(driver)
        login.enter_username("keerthi")
        login.enter_password("keerthi3")
        login.login_button_click()
        login.signup_button_click()

        signup = signupPage(driver)
        signup.enter_details("keerthi","prema","ab@gmail.com","ravi")
        signup.sign_up_button_click()
        time.sleep(20)

    @classmethod
    def tearDownClass(cls):
        cls.driver.close()
        cls.driver.quit()
        print("Test Completed")


    if __name__ == '__main__':
        unittest.main(testRunner=HtmlTestRunner.HtmlTestRunner(output='C:\Users\Admin\PycharmProjects\BasicsPython\reports'))

