from demo.Locators.locators import locatorsfile
class loginPage():

    def __init__(self,driver):
        self.driver = driver
        self.username_textbox_id = locatorsfile.username_textbox_id
        self.password_textbox_id = locatorsfile.password_textbox_id
        self.login_button_id     = locatorsfile.login_button_id
        self.signup_button_link_text = locatorsfile.signup_button_link_text

    def enter_username(self,username):
        self.driver.find_element_by_id(self.username_textbox_id).clear()
        self.driver.find_element_by_id(self.username_textbox_id).send_keys(username)

    def enter_password(self,password):
        self.driver.find_element_by_id(self.password_textbox_id).clear()
        self.driver.find_element_by_id(self.password_textbox_id).send_keys(password)

    def login_button_click(self):
        self.driver.find_element_by_id(self.login_button_id).click()

    def signup_button_click(self):
        self.driver.find_element_by_link_text(self.signup_button_link_text).click()