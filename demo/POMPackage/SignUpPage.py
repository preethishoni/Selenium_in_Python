from demo.Locators.locators import locatorsfile
class signupPage():

    def __init__(self,driver):
        self.driver = driver
        self.firstname_textbox_id = locatorsfile.firstname_textbox_id
        self.surname_textbox_id   = locatorsfile.surname_textbox_id
        self.email_textbox_id     = locatorsfile.email_textbox_id
        self.newpassword_textbox_id = locatorsfile.newpassword_textbox_id
        self.birthday_day           = "day"
        self.birthday_month         = "month"
        self.birthday_year          = "year"
        self.sex_female             = ""
        self.sex_male               = ""
        self.signup_buttonclick     = "websubmit"

    def enter_details(self,firstname,lastname,email,password):
        self.driver.find_element_by_name(self.firstname_textbox_id).send_keys(firstname)
        self.driver.find_element_by_name(self.surname_textbox_id).send_keys(lastname)
        self.driver.find_element_by_name(self.email_textbox_id).send_keys(email)
        self.driver.find_element_by_name(self.newpassword_textbox_id).send_keys(password)

    def sign_up_button_click(self):
        self.driver.find_element_by_link_text(self.signup_buttonclick).click()


