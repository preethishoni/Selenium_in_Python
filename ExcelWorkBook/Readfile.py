import openpyxl

vk = openpyxl.load_workbook("E:\\ExcelWorkBook_Python.xlsx")

print(vk.sheetnames)
print(vk.active.title)
# to read all values in excelsheet
sh = vk['Sheet1']
print(sh.title)

rows = sh.max_row
columns = sh.max_column

print(rows,columns)

for i in range(1,rows+1):
    for j in range(1,columns+1):
        print(sh.cell(i,j).value)


for r in sh['A1':'C5']:
    for c in r:
        print(c.value)


#c1 = sh.cell(1,1)  #cell(row,column)
#print(c1.value)

# can also be written as keyword argument

#c2 = sh.cell(column=1, row=3)
#print(c2)
#print(c2.column,c2.row)

#r1 = sh['A4'].value
#print(r1)